using System.Linq.Expressions;
using Automatonymous;
using Contracts;
using Contracts.Enums;
using MassTransit;

public sealed class BuyItemsSaga : MassTransitStateMachine<BuyItemsSagaState>
{
    private readonly ILogger<BuyItemsSaga> _logger;

    public BuyItemsSaga(ILogger<BuyItemsSaga> logger)
    {
        _logger = logger;
        InstanceState(x => x.CurrentState);
        Event<BuyItemsRequest>(() => BuyItems, x => x.CorrelateById(y => y.Message.OrderId));

        Request(
            () => GetMoney
            );
        Request(
         () => GetItems
         );
        Request(
         () => ReturnMoney
         );

        Initially(
            When(BuyItems)
            .Then(x =>
            {
                if (!x.TryGetPayload(out SagaConsumeContext<BuyItemsSagaState, BuyItemsRequest> payload))
                    throw new Exception("Unable to retrieve required payload for callback data.");
                x.Saga.RequestId = payload.RequestId;
                x.Saga.ResponseAddress = payload.ResponseAddress;
            })
            .Request(GetMoney, x => {
                
                    return x.Init<IGetMoneyRequest>(new { OrderId = x.Message.OrderId });
                })
            .TransitionTo(GetMoney.Pending)

            );

        During(GetMoney.Pending,
            When(GetMoney.Completed)
            .Request(GetItems, x => x.Init<IGetItemsRequest>(new { OrderId = x.Message.OrderId }))
            .TransitionTo(GetItems.Pending),

            When(GetMoney.Faulted)
              .ThenAsync(async context =>
              {
                  await RespondFromSaga(
                      BuyItemsStatusEnum.FailGetMoney,
                      context,
                      "Faulted On Get Money " + string.Join("; ",
                      context.Message.Exceptions.Select(x => x.Message)));
              })
            .TransitionTo(Failed),

            When(GetMoney.TimeoutExpired)
               .ThenAsync(async context =>
               {
                   await RespondFromSaga(BuyItemsStatusEnum.FailGetMoneyTimeout, context, "Timeout Expired On Get Money");
               })
            .TransitionTo(Failed)

             );

        During(GetItems.Pending,
            When(GetItems.Completed)
              .ThenAsync(async context =>
              {
                  await RespondFromSaga(BuyItemsStatusEnum.Success, context, null);
              })
            .Finalize(),

            When(GetItems.Faulted)
              .Request(ReturnMoney, x => x.Init<IReturnMoneyRequest>(new { OrderId = x.Message.Message.OrderId }))
            .TransitionTo(ReturnMoney.Pending),

            When(GetItems.TimeoutExpired)
               .ThenAsync(async context =>
               {
                   await RespondFromSaga(BuyItemsStatusEnum.FailGetItemsTimeout, context, "Timeout Expired On Get Items");
               })
            .TransitionTo(Failed)

            );

        During(ReturnMoney.Pending,

            When(ReturnMoney.Completed)
              .ThenAsync(async context =>
              {
                  await RespondFromSaga(BuyItemsStatusEnum.CanceledMoneyReturned, context, null);
              })
            .Finalize(),

            When(ReturnMoney.Faulted)
              .Request(ReturnMoney, x => x.Init<IReturnMoneyRequest>(new { OrderId = x.Message.Message.OrderId }))
              .ThenAsync(async context =>
              {
                  await RespondFromSaga(BuyItemsStatusEnum.CanceledFailReturnMoney, context, $"Faulted On {nameof(ReturnMoney)} " + string.Join("; ", context.Data.Exceptions.Select(x => x.Message)));
              })
            .TransitionTo(Failed),

            When(ReturnMoney.TimeoutExpired)
               .ThenAsync(async context =>
               {
                   await RespondFromSaga(BuyItemsStatusEnum.CanceledFailReturnMoneyTimeout, context, $"Timeout Expired On {nameof(ReturnMoney)}");
               })
            .TransitionTo(Failed)

            );
    }

    public Request<BuyItemsSagaState, IGetMoneyRequest, IGetMoneyResponse> GetMoney { get; set; }
    public Request<BuyItemsSagaState, IReturnMoneyRequest, IReturnMoneyRequest> ReturnMoney { get; set; }

    public Request<BuyItemsSagaState, IGetItemsRequest, IGetItemsResponse> GetItems { get; set; }
    public Event<BuyItemsRequest> BuyItems { get; set; }
    public State Failed { get; set; }

    private static async Task RespondFromSaga<T>(BuyItemsStatusEnum status, BehaviorContext<BuyItemsSagaState, T> context, string error) where T : class
    {
        var endpoint = await context.GetSendEndpoint(context.Saga.ResponseAddress);
        await endpoint.Send(new BuyItemsResponse
        {
            OrderId = context.Saga.CorrelationId,
            ErrorMessage = error,
            Status = status
        }, r => r.RequestId = context.Saga.RequestId);
    }
}
