using Contracts;
using Contracts.Enums;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace ApiGateway.Controllers
{
    [ApiController]
    [Route("api/v1/items")]
    public class ItemsController : ControllerBase
    {
        private readonly IBus _bus;
        private readonly ILogger<ItemsController> logger;

        public ItemsController(IBus bus, ILogger<ItemsController> logger)
        {
            _bus = bus;
            this.logger = logger;
        }

        [HttpPost("buy")]
        public async Task<BuyItemsResponse> BuyAsync(BuyItemsRequstModel model)
        {
            model.OrderId = Guid.NewGuid();

            logger.LogWarning("HTTTTTT!!!");
            var response = await _bus.Request<BuyItemsRequest, BuyItemsResponse>(model);
            
            GetBuyItemsStatusMessage(response.Message);

            return response.Message;
        }

        private static void GetBuyItemsStatusMessage(BuyItemsResponse response)
        {
            var message = response.Status switch
            {
                BuyItemsStatusEnum.Success => "Покупка совершена успешно",
                BuyItemsStatusEnum.CanceledMoneyReturned => "Покупка не совершена деньги вернулись на счет",
                BuyItemsStatusEnum.CanceledFailReturnMoney => "Покупка не совершена деньги списаны но не возвращены из-за ошибки",
                BuyItemsStatusEnum.CanceledFailReturnMoneyTimeout => "Покупка не совершена деньги списаны но не возвращены из-за внутренней ошибки",
                BuyItemsStatusEnum.FailGetItems => "Покупка не совершена, проблемы с получением товара",
                BuyItemsStatusEnum.FailGetItemsTimeout => "Покупка не совершена, внутренние проблемы с получением товара",
                BuyItemsStatusEnum.FailGetMoney => "Покупка не совершена, проблемы со списанием денег",
                BuyItemsStatusEnum.FailGetMoneyTimeout => "Покупка не совершена, внутренние проблемы со списанием денег",
                _ => "Статус неизвестен"
            };

            response.StatusMessage = message;
        }
    }
}
