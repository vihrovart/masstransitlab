using Contracts;
using MassTransit;

namespace MoneyMicroservice.Consumers
{
    public class ReturnMoneyConsumer : IConsumer<IReturnMoneyRequest>
    {
        public Task Consume(ConsumeContext<IReturnMoneyRequest> context)
        {
            return context.RespondAsync<IReturnMoneyResponse>(new { context.Message.OrderId });
        }
    }
}
