using Contracts;
using MassTransit;
namespace ItemsMicroservice.Consumers
{

    public class GetItemsConsumer : IConsumer<IGetItemsRequest>
    {
        public Task Consume(ConsumeContext<IGetItemsRequest> context)
        {
            throw new ApplicationException("Have no enough items");

            return context.RespondAsync<IGetItemsResponse>(new { OrderId = context.Message.OrderId });
        }
    }
}
