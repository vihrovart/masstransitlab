using System;
namespace Contracts.Enums
{
    public enum BuyItemsStatusEnum
    {
        Success,
        CanceledMoneyReturned,
        CanceledFailReturnMoney,
        CanceledFailReturnMoneyTimeout,
        FailGetItems,
        FailGetItemsTimeout,
        FailGetMoney,
        FailGetMoneyTimeout,
    }
}

