﻿namespace Contracts
{
    public interface IReturnMoneyResponse
    {
        public Guid OrderId { get; }
    }
}
