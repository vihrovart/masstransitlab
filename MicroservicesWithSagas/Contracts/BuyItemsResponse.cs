using Contracts.Enums;

namespace Contracts
{
    public class BuyItemsResponse
    {
        public Guid OrderId { get; set; }
        public string ErrorMessage { get; set; }
        public BuyItemsStatusEnum Status { get; set; }
        public string StatusMessage { get; set; }
    }
}
