namespace Contracts
{
    public interface IReturnMoneyRequest
    {
        public Guid OrderId { get; }
    }
}
