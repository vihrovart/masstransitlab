using System;
namespace Contracts.Configuration
{
    public class RabbitMqConfig
    {
        /// <summary>
        /// Виртуальный хост.
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// Адрес хоста.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Порт
        /// </summary>
        public int? Port { get; set; }

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }
    }
}

